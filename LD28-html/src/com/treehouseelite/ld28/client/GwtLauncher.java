package com.treehouseelite.ld28.client;

import com.treehouseelite.ld28.LD28;
import com.treehouseelite.ld28.Ref;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.gwt.GwtApplication;
import com.badlogic.gdx.backends.gwt.GwtApplicationConfiguration;

public class GwtLauncher extends GwtApplication {
	@Override
	public GwtApplicationConfiguration getConfig () {
		GwtApplicationConfiguration cfg = new GwtApplicationConfiguration(Ref.WIDTH, Ref.HEIGHT);
		return cfg;
	}

	@Override
	public ApplicationListener getApplicationListener () {
		return new LD28();
	}
}