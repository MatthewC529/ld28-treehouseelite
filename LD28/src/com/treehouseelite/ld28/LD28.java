package com.treehouseelite.ld28;
import com.badlogic.gdx.Game;
import com.treehouseelite.ld28.screens.GameLevel;
import com.treehouseelite.ld28.screens.MainMenu;


public class LD28 extends Game {

	@Override
	public void create() {
		MainMenu level = new MainMenu(this);
		Ref.GAME = this;
		this.setScreen(level);
	}

}
