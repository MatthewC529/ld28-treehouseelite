package com.treehouseelite.ld28.thespians;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.treehouseelite.ld28.stagehands.InputState;

/**
 * Entity Superclass
 * 
 * @author Matthew Crocco (Treehouse Elite) & Micahel Lucido (Treehouse Elite)
 *
 */
public class Thespian {

	/**
	 * Simple Enumeration of Directions, with String Names just in case. Call getDirection() for direction in string form.
	 * 
	 * @author Matthew Crocco (Treehouse Elite)
	 */
	public enum Direction{
		Up("north"),Down("south"),Left("west"),Right("east"), None("stat");

		private String direction;

		Direction(String direction){
			this.direction = direction;
		}

		public String getDirection(){
			return direction;
		}

	}

	protected Vector2 position; // Entity Position
	protected Vector2 velocity; // Entity Velocity
	protected Vector2 spawnPosition; //Entity Spawn Position
	protected Texture tex; // Entity Texture


	protected Rectangle BoundingBox; // Entity BoundingBox (Rectangle)
	protected Rectangle TrapY; // Entirely for the Player, tracks player within a certain zone for Camera Movement

	protected Direction xDir; // Current Horizontal Direction the player is moving in
	protected Direction yDir; // Current Vertical Direction the player is moving in

	protected boolean visible;
	
	public static Array<Thespian> thespians = new Array<Thespian>(); //Just keeping track of all our thespians.
	public static Array<Platform> platforms = new Array<Platform>(); //Just keeping track of all our platforms.
	public static Array<Collectible> collectibles = new Array<Collectible>(); //Just keeping track of all our collectibles.
	public static Array<Activator> activators = new Array<Activator>(); //Just keeping track of all our activators.
	public static Array<Absorber> absorbers = new Array<Absorber>(); //Just keeping tack of all our absorbers.

	public Thespian(int x, int y, Texture tex){

		position = new Vector2();
		velocity = new Vector2();
		this.tex = tex;
		this.position.x = x; this.position.y = y;
		setBoundingBox();
		if(this instanceof Player) TrapY = new Rectangle(position.x-2*tex.getWidth(),position.y+5*tex.getHeight(),5*tex.getWidth(),6*tex.getHeight());
		visible = true;
		thespians.add(this);
	}
	
	protected void setBoundingBox(){
		BoundingBox = new Rectangle(this.position.x,this.position.y,tex.getWidth(),tex.getHeight());
	}

	public void drawMe(SpriteBatch batch){
		if(!visible) return;
		batch.draw(tex, this.position.x, this.position.y);
	}

	public void update(float deltaTime){

	}

	public void updateRectangle(){
		BoundingBox.setPosition(position);
		if(TrapY.contains(BoundingBox)) return;

		if(BoundingBox.x < TrapY.x) TrapY.x = BoundingBox.x;
		else if(BoundingBox.x + BoundingBox.width > TrapY.x + TrapY.width) TrapY.x = BoundingBox.x+BoundingBox.width-TrapY.width;

		if(BoundingBox.y < TrapY.y) TrapY.y = BoundingBox.y;
		else if(BoundingBox.y + BoundingBox.height > TrapY.y + TrapY.height) TrapY.y = BoundingBox.y + BoundingBox.height - TrapY.height;

	}

	public Rectangle getBoundingBox() {
		return BoundingBox;
	}

	public Rectangle getTrapY() {
		return TrapY;
	}

	public void updateInput(InputState state){

	}

	public Vector2 getPosition() {
		return position;
	}

	public Vector2 getVelocity() {
		return velocity;
	}

	public Direction[] getDirections(){
		return new Direction[]{xDir, yDir};
	}

	public Direction getDirection(char xory){
		if( xory == 'x'){
			return xDir;
		}else if( xory == 'y' ){
			return yDir;
		}
		return null;
	}

	public boolean isColliding(Player p){

		if(!visible) return false;

		boolean bool = false;

		for(int i = 0; i<platforms.size; i++){
			Platform test = platforms.get(i);
			if(p.BoundingBox.overlaps(test.BoundingBox)){
				bool = true;
				break;
			}else if(i == platforms.size){
				bool = false;
				break;
			}
		}

		return bool;
	}


	public Platform getOffendingShape(Player prosecutor){
		Platform defendant = null;

		for(int j = 0; j<platforms.size;j++){
			if(prosecutor.BoundingBox.overlaps(platforms.get(j).BoundingBox)){
				defendant = platforms.get(j);
				break;
			}
		}

		if(defendant != null){
			return defendant;
		}else{
			return null;
		}
	}
	
	public Texture getTexture(){
		return tex;
	}
	
	public static void bleach(){
		thespians = new Array<Thespian>();
		platforms = new Array<Platform>(); 
		collectibles = new Array<Collectible>(); 
		activators = new Array<Activator>();
		absorbers = new Array<Absorber>();
	}

}
