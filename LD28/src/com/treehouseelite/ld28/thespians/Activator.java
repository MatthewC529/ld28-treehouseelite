package com.treehouseelite.ld28.thespians;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.treehouseelite.ld28.Ref;
import com.treehouseelite.ld28.util.Colour;

public class Activator extends Thespian {

	Colour c;

	private ActivatedPlatform effector;
	private boolean hasEffector = false;

	public Activator(int x, int y, String loc, Colour c) {
		super(x, y, new Texture(loc+String.format("_%s.png", 0)));
		this.c = c;
		activators.add(this);
		
	}

	public Colour getColour(){return c;}
	public void activate(){
		tex = new Texture(Gdx.files.internal(Ref.ACTIVATORTEXTURE+String.format("_%s.png", 1)));
		if(hasEffector){
			effector.activate();
		}
	}

	public void addEffector(ActivatedPlatform p){
		hasEffector = true;
		effector = p;
	}

	public boolean hasEffector(){
		return hasEffector;
	}
}
