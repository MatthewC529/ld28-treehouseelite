package com.treehouseelite.ld28.thespians;

import com.badlogic.gdx.graphics.Texture;
import com.treehouseelite.ld28.util.exceptions.NoActivatorException;

public class ActivatedPlatform extends Platform {

	public int range;
	public int speed;

	int currentDistance = 0;
	Direction dir;
	boolean activated = false;

	public ActivatedPlatform(int x, int y, int range, int speed, Texture tex, Direction d){
		super(x, y, tex);
		this.range = range*32;
		this.speed = speed;
		dir = d;
		if(!attachToActivator()){
			
		}
	}

	@Override
	public void update(float delta){
		if(activated && currentDistance != range){
			switch(dir){
			case Up: velocity.y += speed; currentDistance++;
			case Down: velocity.y -= speed; currentDistance++;
			case Left: velocity.x -= speed; currentDistance++;
			case Right: velocity.x += speed; currentDistance++;
			}

			velocity.scl(delta);
			position.add(velocity);

			updateRectangle();
		}

	}

	private boolean attachToActivator(){
		for(Activator a: activators){
			if(!a.hasEffector()){
				a.addEffector(this);
				return true;
			}
		}
		return false;
	}

	public void activate(){
		activated = true;
	}

}
