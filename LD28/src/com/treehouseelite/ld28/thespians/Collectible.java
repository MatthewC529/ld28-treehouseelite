package com.treehouseelite.ld28.thespians;

import com.badlogic.gdx.graphics.Texture;
import com.treehouseelite.ld28.util.Colour;

public class Collectible extends Thespian {

	private Colour Colour;
	private int pointValue;

	public Collectible(int x, int y, Texture tex,Colour c,int pv) {
		super(x, y, tex);
		Colour = c;
		pointValue = pv;
		collectibles.add(this);
	}

	public Collectible(int x, int y, Texture tex, int pv){
		this(x, y, tex, new Colour(), pv);
	}

	public Colour getColour(){return Colour;}
	public int getPointValue(){return pointValue;}
	public boolean getVisibliilty(){return visible;}
	public void setVisibility(boolean in){
		visible = in;
		
		}

	public void setVisibility(boolean b, Player marco) {
		if(visible) marco.addScore(pointValue);
		visible = b;
	}

}
