package com.treehouseelite.ld28.thespians;

import com.badlogic.gdx.graphics.Texture;

/**
 * Mobile Platform
 * 
 * @author Michael Lucido (Treehouse Elite)
 *
 */
public class MovingPlatform extends Platform {


	public int range; //How far away the platform should travel
	public int speed; //How fast the platform should move.

	int currentDistance; //It's current position
	Direction direc; //Direction
	boolean restep; //Whether or not it's travelling backwards or not.

	public MovingPlatform(int x, int y, Texture tex, Direction d) {
		super(x, y, tex);
		range = 96;
		speed = 64;
		currentDistance = 0;
		direc = d;

	}

	@Override
	public void update(float deltaTime){
		if(restep){
			switch(direc){
			case Up: velocity.y -= speed; currentDistance -= 1; break;
			case Down: velocity.y += speed; currentDistance -= 1; break;
			case Left: velocity.x += speed; currentDistance -= 1; break;
			case Right: velocity.x -= speed; currentDistance -= 1; break;
			}
		}else{
			switch(direc){
			case Up: velocity.y += speed; currentDistance += 1; break;
			case Down: velocity.y -= speed; currentDistance += 1; break;
			case Left: velocity.x -= speed; currentDistance += 1; break;
			case Right: velocity.x += speed; currentDistance += 1; break;
			}
		}

		velocity.scl(deltaTime);
		position.add(velocity);
		BoundingBox.setPosition(position);

		if(currentDistance == range){
			restep = true;
		}

		if(currentDistance == 0){
			restep = false;
		}

		
	}

}
