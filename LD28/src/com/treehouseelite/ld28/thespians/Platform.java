package com.treehouseelite.ld28.thespians;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.treehouseelite.ld28.util.Colour;

/**
 * Stationary Platform
 * 
 * @author Matthew Crocco (Treehouse Elite) & Michael Lucido (Treehouse Elite)
 *
 */
public class Platform extends Thespian {

	public Platform(int x, int y, Texture tex) {
		super(x, y, tex);
		platforms.add(this);
	}

	public Platform(int x, int y,int width, int height, Colour color){
		this(x, y, assemblePixmap(width, height, color));
	}


	private static Texture assemblePixmap(int width, int height, Colour color){
		Pixmap p = new Pixmap(width, height, Format.RGBA8888);
		p.setColor(color);
		return new Texture(p);
	}

}
