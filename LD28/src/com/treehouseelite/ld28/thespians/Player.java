package com.treehouseelite.ld28.thespians;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.treehouseelite.ld28.Ref;
import com.treehouseelite.ld28.stagehands.InputState;
import com.treehouseelite.ld28.util.Timer;

/**
 * Lets make ourselves a playa.
 * 
 * @author Matthew Crocco (Treehouse Elite) & Michael Lucido (Treehouse Elite)
 *
 */
public class Player extends Thespian {

	private final int MOVECONSTANT = 350; // Speed at which one moves in the horizontal directions.
	private final int JUMPCONSTANT = 500; // Speed at which one moves upwards when jumping

	private boolean canJump; // If the player is grounded or on a platform, let him jump please!
	private boolean grounded; // Is the player grounded? Sort of replaced by supported() but this is in field form!
	public boolean isJumping = false; //Is the Player Jumping? I dunno....
	private boolean needToFall = false; //Declared if the Player MUST fall, it is a forced call to fall().

	//Collision Points
	public float x, x2, x3, x4, x5, x6;
	public float y, y2, y3, y4, y5, y6;
	//

	private int speedModifier = 0;
	private int jumpModifier = 0;

	public long flyTime = 500; //Fly Time in Milliseconds
	public Timer jumpTimer = new Timer(); //Timer Handling the Jump Length, Current It is an IMMEDIATE effect
	public int scorePoints = 0;
	
	private Vector2 spawnLocation;

	public Array<Vector2> oldPositions = new Array<Vector2>(1);

	public Player(int x, int y){
		super(x,y,new Texture(Ref.PLAYERTEXTURE));
		spawnLocation = new Vector2(x,y);
	}
	
	public Player(Vector2 in){
		super((int)in.x, (int)in.y, new Texture(Ref.PLAYERTEXTURE));
		spawnLocation = in;
	}


	@Override
	public void update(float deltaTime){
		if(deltaTime == 0) return; // Apparently to prevent crashes?

		//All the brilliant various collision points;
		x2 = position.x+ BoundingBox.width - 6;y2 = position.y+2;	//bottomRight
		x = position.x+6;y = position.y+2;	//bottomLeft
		x3 = position.x + BoundingBox.width+4;y3 = position.y + BoundingBox.height/3;		//Mid-Right
		x4 = position.x + BoundingBox.width-6;y4 = position.y + BoundingBox.height;		//topRight
		x5 = position.x+6; y5 = position.y + BoundingBox.height; //topLeft
		x6 = position.x-4; y6 = position.y+BoundingBox.height/3; //MidLeft



		if( !supported()){
			velocity.add(0, -250f);
		}

		velocity.scl(deltaTime);

		position.add(velocity);
		oldPositions.add(position);

		grounded = supported();

		if(isJumping){
			jump();
		}

		if(needToFall){
			fall();
		}

		this.updateDirection();
		checkContainment();
		
		if(position.y < 0 ){
			respawn();
		}
	}


	@Override
	public void updateInput(InputState state){
		if((state.isKeyPressed(Keys.RIGHT) || state.isKeyPressed(Keys.D)) && moveable(Direction.Right)){
			velocity.x = MOVECONSTANT + speedModifier;
		}else if(!moveable(Direction.Right)) position.x -=1;
		if((state.isKeyPressed(Keys.LEFT) || state.isKeyPressed(Keys.A)) && moveable(Direction.Left)){
			velocity.x = -(MOVECONSTANT + speedModifier);
		}else if(!moveable(Direction.Left)) position.x += 1;

		if((state.isKeyPressed(Keys.SPACE) || state.isKeyPressed(Keys.W)) && supported()){
			grounded = false;
			isJumping = true;
		}
		
		if(state.isKeyPressed(Keys.S) && isJumping){
			isJumping = false;
		}

		/*if((state.isKeyPressed(Keys.SPACE) || state.isKeyPressed(Keys.W)) && supported()){
			System.out.println("HELD DOWN");
			beingHeldDown = true;
			timesChecked++;
			System.out.println(timesChecked);
		}

		if(!state.isKeyPressed((Keys.SPACE)) && beingHeldDown){
			System.out.println("Released");
			beingHeldDown = false;
			lastChecked = timesChecked;
			if(supported()){
				grounded = false;
				isJumping = true;
			}
		}*/


		if(state.isKeyPressed(Keys.ALT_LEFT)){
			Ref.DEBUG = !Ref.DEBUG;
		}
		if(state.isKeyPressed(Keys.ESCAPE)){
			Gdx.app.exit();
		}
		
		if(state.isKeyPressed(Keys.R)) respawn();
	}

	private void jump(){
		if(canJump){
			jumpTimer.time(flyTime);
		}
		if(jumpTimer.timeIsUp()){
			fall();
		}else{
			if(moveable(Direction.Up)){
				velocity.y += JUMPCONSTANT;
			}else{
				position.y -= 5;
				isJumping = false;
				canJump = false;
				grounded = false;
			}
		}
		canJump = false;
	}

	private void fall(){
		if(grounded && !needToFall){
			isJumping=false;
		}else{
			velocity.y -= JUMPCONSTANT + jumpModifier;
			isJumping = false;
		}
	}

	/**
	 * Updates the players Direction in movement, relies on Velocities rather than Change in Position
	 */
	protected void updateDirection(){
		if(this.velocity.y > 0){
			this.yDir = Direction.Up;
		}else if(this.velocity.y <0){
			this.yDir = Direction.Down;
		}else if(this.velocity.y == 0){
			this.yDir = Direction.None;

		}

		if(this.velocity.x > 0){
			this.xDir = Direction.Right;
		}else if(this.velocity.x < 0){
			this.xDir = Direction.Left;
		}else{
			this.xDir = Direction.None;
		}


	}

	public boolean getGrounded(){
		return grounded;
	}



	/**
	 * Check if the player is supported by some kind of Platform or the Floor
	 * 
	 * @author Matthew Crocco (Treehouse Elite)
	 * @return True is supported by platform or floor, False if not
	 */
	public boolean supported(){

		if(!isColliding(this)){
			for(int check = 0; check < platforms.size; check++){
				if(platforms.get(check).getBoundingBox().contains(new Vector2(x,y))|| platforms.get(check).getBoundingBox().contains(new Vector2(x2,y2))){
					Platform platy = platforms.get(check);
					if(this.position.x + BoundingBox.width/2 > platy.position.x || this.position.x + BoundingBox.width/2 < platy.position.x + platy.BoundingBox.width ){
						canJump = false;
						grounded = false;
						if(platy.BoundingBox.contains(x3+6, y3) && this.position.x < platy.position.x){
							this.position.x -= 8;
						}else if(platy.BoundingBox.contains(x6-6, y6) && this.position.x > platy.position.x){
							this.position.x += 8;
						}
						return false;
					}else{
						canJump = true;
						grounded = true;
						return true;
					}
				}
			}
		}

		if(isColliding(this)){
			Platform p = getOffendingShape(this);

			//if(this.position.x < p.position.x || this.position.x+(this.BoundingBox.width/2) > p.position.x + p.BoundingBox.width) return false;
			if(this.position.y > p.position.y){
				canJump = true;
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}

	}

	/**
	 * Get Vertices of the Collision Shape
	 * 
	 * @return Float[] - Array of Vertices in the format {x, y, x2, y2, x3, y3, ...} and so on
	 */
	public float[] getCollisionVertices(){
		return new float[]{x, y, x2, y2, x3, y3, x4, y4, x5,y5,x6,y6};
	}

	/**
	 * Checks if an obstacle is in the way of the desired HORIZONTAL direction or not.
	 * 
	 * @param dir - Desired direction
	 * @return True if moveable in that direction, false if there is an obstacle in the collision shape.
	 */
	private boolean moveable(Direction dir){ //Moveable is a variant of Movable! Eclipse stop trying to correct me!
		if(dir.equals(Direction.Right)){
			for(Platform p: platforms){
				Rectangle test = p.BoundingBox;
				if(test.contains(x3, y3)){
					return false;
				}
			}
			return true;
		}else if(dir.equals(Direction.Left)){
			for(Platform p: platforms){
				Rectangle test = p.BoundingBox;
				if(test.contains(x6, y6)){
					return false;
				}
			}
			return true;
		}else if(dir.equals(Direction.Up)){
			for(Platform p: platforms){
				Rectangle test = p.BoundingBox;
				if(test.contains(x4, y4) || test.contains(x5, y5)){
					return false;
				}
			}
			return true;
		}
		return false;
	}

	public void setSpeedModifier(int in){speedModifier = in;}
	public void setJumpModifiers(int[] in){
		jumpModifier = in[0];
		flyTime += in[1];
	}
	public void addScore(Collectible c){
		if(c.visible){
			scorePoints += c.getPointValue();
		}
	}
	
	public int getScore(){
		return scorePoints;
	}

	private void checkContainment(){
		if(canMove()){

		}else{

			if(!canMove()){
				position = spawnPosition;
				updateRectangle();
			}
		}
	}

	private boolean canMove(){
		if(moveable(Direction.Up)|| moveable(Direction.Down) || moveable(Direction.Left)|| moveable(Direction.Right)  ){
			return true;
		}else{
			return false;
		}
	}
	
	public void respawn(){
		this.position = new Vector2(spawnLocation);
		this.velocity.x = 0;
		this.velocity.y = 0;
		
		isJumping = false;
		needToFall = false;
	}

	public void addScore(int val) {
		scorePoints += val;
	}
}
