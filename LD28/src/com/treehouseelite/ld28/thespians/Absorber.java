package com.treehouseelite.ld28.thespians;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.treehouseelite.ld28.Ref;
import com.treehouseelite.ld28.stagehands.Animator;
import com.treehouseelite.ld28.util.Colour;

public class Absorber extends Thespian{

	private Animation anim;
	private Color color;
	private float stateTime = 0;
	
	public Absorber(int x, int y, Texture tex, Color black) {
		super(x, y, tex);
		color = black;
		absorbers.add(this);
		anim = Animator.createAnimation(new Texture(Ref.ABSORBERANIMATION), 32, .05f);
	}
	
	public Color getColor(){return color;}
	@Override
	public void drawMe(SpriteBatch batch){
		stateTime += Gdx.graphics.getDeltaTime();
		anim.setPlayMode(Animation.LOOP_RANDOM);
		batch.draw(anim.getKeyFrame(stateTime), this.position.x, this.position.y);

	}
	@Override
	protected void setBoundingBox(){
		BoundingBox = new Rectangle(this.position.x+tex.getWidth()/4, this.position.y + tex.getWidth()/4,tex.getWidth()/2, tex.getHeight()/2);
	}
}
