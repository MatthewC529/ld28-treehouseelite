package com.treehouseelite.ld28.util.exceptions;

import com.badlogic.gdx.utils.GdxRuntimeException;

public class InvalidFrameException extends GdxRuntimeException{

	public InvalidFrameException(String message) {
		super(message);
	}
	
	public InvalidFrameException(Throwable t){
		super(t);
	}

	public InvalidFrameException(Throwable t, String msg){
		super(msg, t);
	}
	
	public InvalidFrameException(){
		super("Animation Frames Must be a Power of 2 (2, 4, 8, 16, 32, ...) and No Larger Than 64 pixels!");
	}
}
