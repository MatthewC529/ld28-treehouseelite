package com.treehouseelite.ld28.util.exceptions;


public final class NoActivatorException extends RuntimeException{

	public NoActivatorException(String message){
		super(message);
	}

	public NoActivatorException(Throwable t){
		super(t);
	}

	public NoActivatorException(String message, Throwable t){
		super(message, t);
	}
}
