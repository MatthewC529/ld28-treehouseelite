package com.treehouseelite.ld28.util;

import com.badlogic.gdx.graphics.Color;

/**
 * The best class of the entire project clearly. Extends libGDX's Color Class, main use is for our own hexadecimal values.
 * The ultimate part is that hexadecimals can now be used! Woo!
 * 
 * This class in unextendable at the moment.
 * 
 * @author Matthew Crocco (Treehouse Elite)
 *
 */
public final class Colour extends Color{

	public int val8;


	/**
	 * Passing no Parameters defaults to White
	 */
	public Colour(){
		super(Color.WHITE);
		deriveValue();
	}

	/**
	 * Creates a color based on 4 Float values, Red-ness, Green-ness, Blue-ness and Alpha...-ness (Opacity). Values are from [0-1].
	 * 
	 * @param r - Red Value
	 * @param g - Green Value
	 * @param b - Blue Value
	 * @param a - Alpha/Opacity Value
	 */
	public Colour(float r, float g, float b, float a){
		super(r, g, b, a);
		deriveValue();
	}

	/**
	 * Creates a color based on 3 Float values, Red-ness, Green-ness and Blue-ness but defaults to a solid color (Alpha = 1.0f/Full Opacity).
	 * Values are from [0-1].
	 * 
	 * @param r - Red Value
	 * @param g - Green Value
	 * @param b - Blue Value
	 */
	public Colour(float r, float g, float b){
		this(r, g, b, 1.0f);
		deriveValue();
	}

	/**
	 * For those with more specific tastes. Creates a color from a specified Hexadecimal String and Alpha Float value. Alpha value ranges from
	 * [0-1].
	 * 
	 * @param hexValue - Hexadecimal String for the desired Color
	 * @param a - Alpha Value/Opacity
	 */
	public Colour(String hexValue, float a){
		super(convertHex(hexValue, a));
		deriveValue();
	}

	/**
	 * For those with more specific tastes. Creates a color from a specified Hexadecimal String but defaults to Alpha = 1.0f or full opacity.
	 * 
	 * @param hexValue - Hexadecimal String for the desired Color
	 */
	public Colour(String hexValue){
		this(hexValue, 1.0f);
		deriveValue();
	}

	private static final int convertHex(String hex, float a){
		StringBuilder sb = new StringBuilder(hex); // This was amazing when I discovered it.

		if(hex.contains("#")){//Get rid of that hashtag!
			sb.deleteCharAt(0);
			hex = sb.toString();
			hex.trim();// No Whitespace Here sir... Full Efficiency!
		}

		// Conversion from Hexadecimal to 32-bit Floating Point (Decimal)
		float r = (float)Integer.valueOf(hex.substring(0, 2), 16)/255f;
		float g = (float)Integer.valueOf(hex.substring(2,4), 16)/255f;
		float b = (float)Integer.valueOf(hex.substring(4,6), 16)/255f;

		return rgba8888(r, g, b, a); //LibGDX Color Class method for conversion to a singular Integer value
	}

	private void deriveValue(){
		val8 = Color.rgba8888(this);
	}
}
