package com.treehouseelite.ld28.stagehands;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.treehouseelite.ld28.Ref;
import com.treehouseelite.ld28.thespians.Absorber;
import com.treehouseelite.ld28.thespians.ActivatedPlatform;
import com.treehouseelite.ld28.thespians.Activator;
import com.treehouseelite.ld28.thespians.Collectible;
import com.treehouseelite.ld28.thespians.MovingPlatform;
import com.treehouseelite.ld28.thespians.Platform;
import com.treehouseelite.ld28.thespians.Player;
import com.treehouseelite.ld28.thespians.Thespian.Direction;
import com.treehouseelite.ld28.util.Colour;

/**
 * World Generator
 * 
 * @author Michael Lucido (Treehouse Elite) & Matthew Crocco (Treehouse Elite)
 *
 */
public final class World extends Pixmap {

	//------------------------------------------------------------
	//WORLD IDS
	public static final Colour STATIONARY = new Colour("FFFFFF");
	public static final Colour MOVERIGHT = new Colour("FF6A00");
	public static final Colour ACTIVATOR = new Colour("00FFFF");
	public static final Colour REDPICKUP = new Colour("FF0000");
	public static final Colour CRYSTAL = new Colour("FF7FED");
	public static final Colour ACTIVATED = new Colour("66CCCC");
	public static final Colour PRISM = new Colour("0094FF");
	public static final Colour ABSORBER = new Colour("7F0037");
	public static final Colour SPAWNER = new Colour("FF006E");
	//------------------------------------------------------------

	int temp;
	Vector2 spawnLocation; 

	public World(FileHandle file) {
		super(file);
	}

	public World(int width, int height, Format format) {
		super(width, height, format);
	}

	public void populate(){
		for(int y = 0; y < this.getHeight(); y++){
			for(int x = 0; x < this.getWidth(); x++){
				temp = this.getPixel(x, y);
				if(temp == STATIONARY.val8) new Platform(x*32,y*32,new Texture(Ref.TILETEXTURE));
				if(temp == MOVERIGHT.val8) new MovingPlatform(x*32,y*32,new Texture(Ref.TILETEXTURE),Direction.Right); // Woo Enums!
				if(temp == REDPICKUP.val8) new Collectible(x*32,y*32,new Texture(Ref.COLLECTIBLETEXTURE),REDPICKUP, Ref.POWERUPVAL);
				if(temp == ACTIVATOR.val8) new Activator(x*32, y*32, Ref.ACTIVATORTEXTURE, ACTIVATOR);
				if(temp == CRYSTAL.val8) new Collectible(x*32, y*32, new Texture(Ref.CRYSTALTEXTURE), Ref.CRYSTALVAL);
				if(temp == PRISM.val8) new Collectible(x*32, y*23, new Texture(Ref.PRISMTEXTURE), Ref.PRISMPVAL);
				if(temp == ACTIVATED.val8) new ActivatedPlatform(x*32, y*32, 3, 64, new Texture(Ref.TILETEXTURE), Direction.Down);
				if(temp == ABSORBER.val8) new Absorber(x*32, y*32, new Texture(Ref.ABSORBERTEXTURE), Colour.BLACK);
				if(temp == SPAWNER.val8 ) spawnLocation = new Vector2(x*32,y*32);
			}
		}
	}
	
	public Player spawnPlayer(){
		return new Player(spawnLocation);
	}
	
	



}
