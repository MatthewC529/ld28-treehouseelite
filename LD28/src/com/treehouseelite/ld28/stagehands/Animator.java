package com.treehouseelite.ld28.stagehands;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.treehouseelite.ld28.util.exceptions.InvalidFrameException;

/**
 * Singleton Class to produce Animations from classic SpriteSheets
 * 
 * @author Matthew Crocco (Treehouse Elite)
 *
 */
public final class Animator {
	
	private static int NUM_COL;
	private static int NUM_ROW;
	
	private static Texture sheet;
	private static TextureRegion[] keyFrames;
	
	/**
	 * Pass in a SpriteSheet to be made into an animation. The frame sizes are automatically calculated in this method.
	 * 
	 * @param spriteSheet
	 * @param columns
	 * @param rows
	 * @param fps
	 * @return
	 */
	public static Animation createAnimation(Texture spriteSheet, int rows, int columns, float fps){
		NUM_COL = columns; NUM_ROW = rows; sheet = spriteSheet;
		TextureRegion[][] temp = TextureRegion.split(sheet, sheet.getWidth()/NUM_COL, sheet.getHeight()/NUM_ROW);
		keyFrames = new TextureRegion[NUM_COL*NUM_ROW];
		
		int index =0;
		for(int i = 0; i<NUM_ROW; i++){
			for(int j=0;j<NUM_COL; j++){
				keyFrames[index] = temp[i][j];
				index++;
			}
		}
		return new Animation(fps, keyFrames);
	}
	
	public static Animation createAnimation(Texture spriteSheet, int frameSize, float fps){
		if(checkFrameSize(frameSize)){
			return createAnimation(spriteSheet, spriteSheet.getWidth()/frameSize, spriteSheet.getHeight()/frameSize, fps);
		}else throw new InvalidFrameException();
	}
	
	private static boolean checkFrameSize(int size){
		int[] checks = {(int) Math.pow(2, 1), (int) Math.pow(2, 2), (int) Math.pow(2, 3), (int) Math.pow(2, 4), (int) Math.pow(2, 5), (int) Math.pow(2, 6)};
		
		for(int i: checks){
			if(size == checks[i]){
				return true;
			}
		}
		return false;
	}
	
}
