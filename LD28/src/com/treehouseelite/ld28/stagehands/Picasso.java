package com.treehouseelite.ld28.stagehands;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.treehouseelite.ld28.Ref;
import com.treehouseelite.ld28.screens.GameLevel;
import com.treehouseelite.ld28.thespians.Absorber;
import com.treehouseelite.ld28.thespians.Activator;
import com.treehouseelite.ld28.thespians.Collectible;
import com.treehouseelite.ld28.thespians.Thespian;
import com.treehouseelite.ld28.util.Colour;

public class Picasso {

	private Colour current;
	private GameLevel game;

	public Picasso(GameLevel level){
		current = new Colour();
		game = level;
	}

	public void paint(SpriteBatch batch){
		for(Collectible c : Thespian.collectibles){
			if(c.getBoundingBox().overlaps(game.Marco.getBoundingBox())){
				if(c.getColour().val8 != World.CRYSTAL.val8 || c.getColour() != Colour.WHITE)current = c.getColour();
				if(c.getVisibliilty()){
					if(current.val8 == World.REDPICKUP.val8){
						game.Marco.addScore(c);
						current = c.getColour();
						game.Marco.setJumpModifiers(new int[]{100, 250}); // Index 0 = Jump Speed Modifier, Index 1 = Fly Time Modifier
					}
					if(current.val8 == World.CRYSTAL.val8){
						game.Marco.addScore(Ref.CRYSTALVAL);
						c.setVisibility(false, game.Marco);
					}
					if(current.val8 == World.PRISM.val8){
						Ref.CURRENTLEVEL++;
						Thespian.bleach();
						Ref.GAME.setScreen(new GameLevel(Ref.GAME));
					}
				}

				c.setVisibility(false);
				if(current.val8 == World.REDPICKUP.val8){
					game.Marco.addScore(c);
					game.Marco.setJumpModifiers(new int[]{100, 250}); // Index 0 = Jump Speed Modifier, Index 1 = Fly Time Modifier
				}else if(current.val8 == World.CRYSTAL.val8){
					game.Marco.addScore(c);
				}
			}
		}

		for(Activator a : Thespian.activators){
			if(a.getBoundingBox().overlaps(game.Marco.getBoundingBox())){
				if(a.getColour()!=Colour.WHITE)current = a.getColour();
				a.activate();
				if(current.val8 == World.ACTIVATOR.val8){
					game.Marco.setSpeedModifier(50);
					a.activate();
				}
			}
		}
		
		for(Absorber abs : Thespian.absorbers){
			if(abs.getBoundingBox().overlaps(game.Marco.getBoundingBox())){
				game.Marco.respawn();
			}
		}
		batch.setColor(current);
	}


	public void setCurrent(Colour in){current = in;}
	public Colour getCurrent(){return current;}
}
