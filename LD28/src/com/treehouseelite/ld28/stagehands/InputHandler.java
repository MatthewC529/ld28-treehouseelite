package com.treehouseelite.ld28.stagehands;

import com.badlogic.gdx.InputProcessor;
import com.treehouseelite.ld28.Ref;

//Recieves Input from user, and translates to Events
public class InputHandler implements InputProcessor {

	private InputState event;
	//----------------------------------------------------------------------------------
	//My Methods

	public InputHandler(){
		event = new InputState();
	}

	public InputState getState(){
		return event;
	}

	//----------------------------------------------------------------------------------
	//Input Methods

	@Override
	public boolean keyDown(int keycode) {
		event.keyDown(keycode);
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		event.keyUp(keycode);
		return false;
	}

	@Override
	public boolean keyTyped(char character) {

		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {

		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {

		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {

		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		event.storeMouse(screenX, -screenY+Ref.HEIGHT);
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		event.scrollAmoung(amount);
		return false;
	}

}
