package com.treehouseelite.ld28.stagehands;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.treehouseelite.ld28.Ref;
import com.treehouseelite.ld28.screens.GameLevel;
import com.treehouseelite.ld28.util.Colour;

public class Salinger extends DebugLayer{
	
	private Vector2 screenTop;
	
	public Salinger(GameLevel level) {
		super(level);
	}
	
	public Salinger(GameLevel level, String fontSrc){
		this(level);
		Oscar = new BitmapFont(Gdx.files.internal(fontSrc));
		Oscar.setColor(Colour.BLACK);
	}
	
	@Override
	public void drawMe(SpriteBatch batch){
		updateVector();
		if(!Ref.DEBUG){
			Oscar.draw(batch, String.format("Score: %s", getPlayerScore()), screenTop.x, screenTop.y);
		}
	}
	
	public void drawMe(SpriteBatch batch, String message){
		drawMe(batch);
		
	}
	
	private String getPlayerScore(){
		String score = Integer.toString(level.Marco.getScore());
		if(score.length() > 11){
			return "l33t Sc0r3"; // heh why not?
		}else{
			return score;
		}
	}
	
	private void updateVector(){
		screenTop = new Vector2(level.getCamera().position.x - level.getCamera().viewportWidth/2+10, level.getCamera().position.y + level.getCamera().viewportHeight/2 - 5);
	}
	
}
