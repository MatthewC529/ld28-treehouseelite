package com.treehouseelite.ld28.stagehands;

import java.text.DecimalFormat;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;
import com.treehouseelite.ld28.Ref;
import com.treehouseelite.ld28.screens.GameLevel;
import com.treehouseelite.ld28.thespians.Absorber;
import com.treehouseelite.ld28.thespians.Platform;
import com.treehouseelite.ld28.thespians.Player;
import com.treehouseelite.ld28.thespians.Thespian;
import com.treehouseelite.ld28.thespians.Thespian.Direction;
import com.treehouseelite.ld28.util.Colour;
/**
 * 
 * @author Matthew Crocco (Treehouse Elite) & Michael Lucido (Treehouse Elite)
 *
 */
public class DebugLayer {

	GameLevel level;
	BitmapFont Oscar;
	ShapeRenderer Lucas; // Lucas is doing a good job :D

	public DebugLayer(GameLevel level){
		this.level = level;
		Oscar = new BitmapFont(Gdx.files.internal("fonts/visitor_24px.fnt"));
		Oscar.setColor(Color.BLACK);
		Lucas = new ShapeRenderer();
	}

	/**
	 * Draw all necessary debug text including Point Values, Booleans, etc.
	 * 
	 * @param batch - SpriteBatch to draw into
	 */
	public void drawMe(SpriteBatch batch){
		DecimalFormat df = new DecimalFormat("##.#######");
		Oscar.draw(batch, "PX: " + level.Marco.getPosition().x + " PY: " + level.Marco.getPosition().y , level.getCamera().position.x+5 - level.getCamera().viewportWidth/2, level.getCamera().position.y + level.getCamera().viewportHeight/2 - Oscar.getCapHeight());
		Oscar.draw(batch, "PvX: " + df.format(level.Marco.getVelocity().x)+ " PvY: " + df.format(level.Marco.getVelocity().y) , level.getCamera().position.x+5 - level.getCamera().viewportWidth/2, level.getCamera().position.y + level.getCamera().viewportHeight/2-(Oscar.getCapHeight()*2)-5);
		Oscar.draw(batch, "bJumping: " + level.Marco.isJumping, level.getCamera().position.x+5 - level.getCamera().viewportWidth/2, level.getCamera().position.y + level.getCamera().viewportHeight/2-(Oscar.getCapHeight()*3)-10);
		Oscar.draw(batch, String.format("pDirs: X-%s Y-%s", getDirection(0), getDirection(1)), level.getCamera().position.x+5 - level.getCamera().viewportWidth/2, level.getCamera().position.y + level.getCamera().viewportHeight/2-(Oscar.getCapHeight()*4)-15);
		Oscar.draw(batch, "pColliding: " + level.Marco.supported(), level.getCamera().position.x+5 - level.getCamera().viewportWidth/2, level.getCamera().position.y + level.getCamera().viewportHeight/2-(Oscar.getCapHeight()*5)-20);
		Oscar.draw(batch, "tDelta: " + Gdx.graphics.getDeltaTime(), level.getCamera().position.x+5 - level.getCamera().viewportWidth/2, level.getCamera().position.y + level.getCamera().viewportHeight/2-(Oscar.getCapHeight()*6)-25);
	}

	/**
	 * Draw all necessary Debug Shapes including platform BoundingBoxes and Collision Shapes.
	 * 
	 * @param batch - SpriteBatch to get Projection Matrix from, to draw in.
	 */
	public void useLucas(SpriteBatch batch){
		if(Ref.DEBUG){
			Lucas.setProjectionMatrix(batch.getProjectionMatrix());
			Lucas.begin(ShapeType.Line);
			Lucas.polygon(level.Marco.getCollisionVertices());
			Lucas.rect(level.Marco.getTrapY().x, level.Marco.getTrapY().y, level.Marco.getTrapY().width, level.Marco.getTrapY().height);
			absorberDebug();
			Lucas.end();
			Lucas.begin(ShapeType.Filled);
			movingDebug();
			Lucas.end();
		}
	}

	private String getDirection(int index){
		Direction[] arr = level.Marco.getDirections();

		return arr[index].getDirection();
	}

	private void movingDebug(){
		Array<Platform> arr = Thespian.platforms;
		for(int i = 0; i < arr.size; i++){
			Rectangle curRect = arr.get(i).getBoundingBox();
			Lucas.setColor(Color.ORANGE);
			Lucas.rect(curRect.x, curRect.y, curRect.width, curRect.height);
		}
	}

	public void write(String message, float x, float y){
		Oscar.setScale(0.5f);
	}

	public void write(String message, Thespian entity){
		write(message, entity.getPosition().x+8, entity.getPosition().y+38);
	}
	
	public void absorberDebug(){
		for(Absorber a: Thespian.absorbers){
			Lucas.setColor(new Colour());
			Lucas.rect(a.getBoundingBox().x, a.getBoundingBox().y, a.getBoundingBox().width, a.getBoundingBox().height);
		}
	}
}
