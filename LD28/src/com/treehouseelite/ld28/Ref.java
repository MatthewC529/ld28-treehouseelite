package com.treehouseelite.ld28;

public final class Ref {

	//General:
	public final static String WINDOWTITLE = "MONOCHROMATIC";
	public final static int WIDTH = 800;
	public final static int HEIGHT = 600;
	public static boolean DEBUG = false;

	public static int CRYSTALVAL = 500;
	public static int POWERUPVAL = 1500;
	public static int PRISMPVAL = 3500;


	//Textures:
	public final static String PLAYERTEXTURE = "Player.png";
	public final static String TILETEXTURE = "Tile.png";
	public final static String COLLECTIBLETEXTURE = "Collectible.png";
	public final static String ACTIVATORTEXTURE = "Activator";
	public final static String CRYSTALTEXTURE = "Crystal.png";
	public final static String PRISMTEXTURE = "Prism.png";
	public final static String ABSORBERTEXTURE = "Absorber.png";
	
	//SpriteSheets
	public final static String CRYSTALANIMATION = "Crystal_Animation.png";
	public final static String ABSORBERANIMATION = "Absorber_Animation.png";
	
	//Current Level
	public static LD28 GAME;
	public static int CURRENTLEVEL = 1;

}
