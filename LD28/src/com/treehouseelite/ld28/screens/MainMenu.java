package com.treehouseelite.ld28.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.treehouseelite.ld28.LD28;
import com.treehouseelite.ld28.Ref;
import com.treehouseelite.ld28.stagehands.InputHandler;
import com.treehouseelite.ld28.stagehands.InputState;

public class MainMenu implements Screen {

	LD28 game;
	OrthographicCamera camera;
	SpriteBatch batch;
	BitmapFont Alfonso;
	BitmapFont Fonzie;
	
	InputHandler Dodgers;
	InputState Duck;
	
	Texture[] collectables;
	Texture[] avoidables;
	
	
	public MainMenu(LD28 game){
		this.game = game;
		camera = new OrthographicCamera();
		camera.setToOrtho(false, Ref.WIDTH, Ref.HEIGHT);
		batch = new SpriteBatch();
		Alfonso = new BitmapFont(Gdx.files.internal("fonts/visitor_64px.fnt"));
		Fonzie = new BitmapFont(Gdx.files.internal("fonts/visitor_24px.fnt"));
		
		Dodgers = new InputHandler();
		Gdx.input.setInputProcessor(Dodgers);
		
		collectables = new Texture[3];
		avoidables = new Texture[3];
		
		collectables[0] = new Texture(Gdx.files.internal("Collectible.png"));
		collectables[1] = new Texture(Gdx.files.internal("Crystal.png"));
		collectables[2] = new Texture(Gdx.files.internal("Prism.png"));
		avoidables[0] = new Texture(Gdx.files.internal("Absorber.png"));

	}
	
	
	@Override
	public void render(float delta) {
		
		Duck = Dodgers.getState();
		
		batch.begin();
			Alfonso.draw(batch, "MONOCHROMATIC", 125, Ref.HEIGHT-100);
			Alfonso.draw(batch, "_____________", 125, Ref.HEIGHT-110); //Ghetto Underlining
			Fonzie.draw(batch, "MOVEMENT: A/D or LeftKey/RightKey", 125, Ref.HEIGHT-200);
			Fonzie.draw(batch, "JUMP: SPACE OR W", 125, Ref.HEIGHT-240);
			Fonzie.draw(batch, "To Stop Jump Mid-Air: S", 125, Ref.HEIGHT-280);
			Fonzie.draw(batch, "COLLECT THESE: ", 125, Ref.HEIGHT-320);
			Fonzie.draw(batch, "AVOID THESE: ", 125, Ref.HEIGHT-360);
			Fonzie.draw(batch, "RESTART: R", 125, Ref.HEIGHT-400);
			Fonzie.draw(batch, "QUIT: Esc", 125, Ref.HEIGHT-440);
			Fonzie.draw(batch, "Press SPACE to Start", 125, Ref.HEIGHT-520);
			
			batch.draw(collectables[0], 350, Ref.HEIGHT-342);
			batch.draw(collectables[1], 400, Ref.HEIGHT-342);
			batch.draw(collectables[2], 450, Ref.HEIGHT-342);
			batch.draw(avoidables[0], 325, Ref.HEIGHT-382);
		batch.end();
		
		if(Duck.isKeyPressed(Keys.SPACE)){
			game.setScreen(new GameLevel(game));
		}
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}
	
}
