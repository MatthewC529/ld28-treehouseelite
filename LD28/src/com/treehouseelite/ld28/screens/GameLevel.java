package com.treehouseelite.ld28.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.treehouseelite.ld28.LD28;
import com.treehouseelite.ld28.Ref;
import com.treehouseelite.ld28.stagehands.DebugLayer;
import com.treehouseelite.ld28.stagehands.InputHandler;
import com.treehouseelite.ld28.stagehands.InputState;
import com.treehouseelite.ld28.stagehands.Picasso;
import com.treehouseelite.ld28.stagehands.Salinger;
import com.treehouseelite.ld28.stagehands.World;
import com.treehouseelite.ld28.thespians.Absorber;
import com.treehouseelite.ld28.thespians.Activator;
import com.treehouseelite.ld28.thespians.Collectible;
import com.treehouseelite.ld28.thespians.Platform;
import com.treehouseelite.ld28.thespians.Player;
import com.treehouseelite.ld28.thespians.Thespian;
import com.treehouseelite.ld28.util.Colour;
import com.treehouseelite.ld28.util.Timer;

public class GameLevel implements Screen {

	LD28 game;
	OrthographicCamera camera;
	SpriteBatch batch;
	InputHandler Billy;
	InputState Budd;
	DebugLayer Carlos;
	Salinger Salinger;
	World test;

	public Player Marco;
	public Picasso Picasso;
	
	private String popUp = "";
	
	private Timer jumpTimer;

	public GameLevel(LD28 game){
		this.game = game;

		camera = new OrthographicCamera();
		camera.setToOrtho(false, Ref.WIDTH, Ref.HEIGHT);
		camera.zoom = 1.0f;

		batch = new SpriteBatch();
		Carlos = new DebugLayer(this);
		Picasso = new Picasso(this);
		Salinger = new Salinger(this);

		test = new World(Gdx.files.internal("Level_"+Ref.CURRENTLEVEL+".png"));
		if(Ref.CURRENTLEVEL == 1){
			test.drawPixel(1, 20, World.MOVERIGHT.val8);
			test.drawPixel(2, 20, World.MOVERIGHT.val8);
			test.drawPixel(3, 29, World.REDPICKUP.val8);
			test.drawPixel(16, 51, World.PRISM.val8);
		}
		test.populate();

		Billy = new InputHandler();
		Gdx.input.setInputProcessor(Billy);

		Marco = test.spawnPlayer();
		
		jumpTimer = new Timer();
		jumpTimer.time(800L);


	}

	//----------------------------------------------------
	//Screen Methods

	@Override
	public void render(float delta) {

		Gdx.gl.glClearColor(Picasso.getCurrent().r,Picasso.getCurrent().g,Picasso.getCurrent().b,Picasso.getCurrent().a);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT); //Clear the Screen

		Budd = Billy.getState();

		camera.update();
		camera.position.set(Marco.getTrapY().x,Marco.getTrapY().y,camera.position.z);

		for(Platform p : Thespian.platforms) p.update(Gdx.graphics.getDeltaTime());
		if(jumpTimer.timeIsUp()) Marco.updateInput(Budd);
		Marco.update(Gdx.graphics.getDeltaTime());
		Marco.updateRectangle();
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		batch.setColor(Picasso.getCurrent());
		for(Activator a : Thespian.activators) a.drawMe(batch);
		for(Absorber ab: Thespian.absorbers) ab.drawMe(batch);
		for(Platform p : Thespian.platforms) p.drawMe(batch);
		for(Collectible c : Thespian.collectibles) c.drawMe(batch);
		Marco.drawMe(batch);
		if(Ref.DEBUG){
			Carlos.useLucas(batch);
			Carlos.drawMe(batch);
		}
		if(!popUp.isEmpty()){
			Salinger.drawMe(batch, popUp);
			popUp = "";
		}else{
			Salinger.drawMe(batch);
		}
		batch.end();
		Picasso.paint(batch);
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}
	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}
	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}
	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}
	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}
	
	public Camera getCamera(){
		return camera;
	}
	
	public void setPopUp(String msg){
		popUp = msg;
	}
}
