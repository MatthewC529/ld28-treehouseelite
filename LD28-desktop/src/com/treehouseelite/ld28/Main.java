package com.treehouseelite.ld28;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.Files.FileType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class Main {
	public static void main(String[] args) {
		
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		
		System.setProperty("org.lwjgl.opengl.Display.allowSoftwareOpenGL", "true");
		
		//Establishes Game Config and What Not.
		cfg.title = Ref.WINDOWTITLE;
		cfg.useGL20 = false;
		cfg.width = Ref.WIDTH;
		cfg.height = Ref.HEIGHT;
		cfg.addIcon("Player.png", FileType.Internal);
		
		cfg.resizable = false;
		
		//Let's get this party started.
		new LwjglApplication(new LD28(), cfg);
	}
}
